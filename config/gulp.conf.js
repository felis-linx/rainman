'use strict';

module.exports = {
  external: {
    materializeCss: './node_modules/materialize-css/sass/ghpages-materialize.scss',
    materializeFont: './node_modules/materialize-css/fonts/**/*.*',
    materializeJs: './node_modules/materialize-css/dist/js/*.min.js',
    materialIconCss: './node_modules/material-design-icons/iconfont/material-icons.css',
    materialIconFont: './node_modules/material-design-icons/iconfont/MaterialIcons-Regular.{eot,woff2,woff,ttf}',
    weatherIconCss: [
      './node_modules/weather-icons/sass/weather-icons.scss',
      './node_modules/weather-icons/sass/weather-icons-wind.scss'
    ],
//    weatherIconCss: './node_modules/weather-icons/css/weather-icons.css',
    weatherIconFon: './node_modules/weather-icons/font/*.{eot,svg,woff2,woff,ttf}'
  },
  front: {
    app: [
      './app/front/*.js',
      './node_modules/materialize-css/dist/js/*.min.js'
    ],
    assets: [
      './app/front/**/*.*',
      '!./app/front/application/**/**',
      '!**/*.js'
    ]
  },
  server: './app/server/**',
//  client: {
//    source: ['client/**/*.{html,css,ico}', '!**/app/**'],
//    destination: 'dist/client',
//    app: ['client/**/*.js']
//  },
//  server: {
//    source: ['server/**/*.{js,json}', '!server/**/*.spec.*'],
//    destination: 'dist/server'
//  },
  destination: {
    root: 'dist',
    front: 'dist/public',
    fonts: 'dist/public/assets/fonts',
    font: 'dist/public/assets/font',
    style: 'dist/public/assets/css',
    js: 'dist/public/assets/js',
  }
};

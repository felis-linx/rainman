"use strict";

var http = require('http');
var fs = require('fs');
var path = require('path');
var pathToRegexp = require('path-to-regexp');
var moment = require('moment');

const port = 3000;
const root = __dirname+'/public';

const fileTypes = {
  '.js':'text/javascript',
  '.css': 'text/css',
  '.json': 'application/json',
  '.png': 'image/png',
  '.jpg': 'image/jpg',
  '.wav': 'audio/wav'
};

const citiesList = [
  {code: 0x01, name: 'Cabo de Hornos'}, //, country: 152 
  {code: 0x03, name: 'Moscow'}, //, country: 643 
  {code: 0x0b, name: 'Lukla'} //, country: 524
];

const wind = ['n', 'ne', 'e', 'se', 's', 'sw', 'w', 'nw'];
const falltype = ['rain', 'sleet', 'snow'];

const weatherSample = {
  date: moment().format('YYYY-MM-DD'),
  temperature: 15,
  fall: 0.5,
  falltype: 'rain',
  pressure: 750,
  humidity: 93,
  wind: 'ne',
  windspeed: 10,
  cloudiness: 70,
  storm: null
};
      
const httpMsg = {
  404: 'Not found',
  405: 'Method Not Allowed',
  423: 'Locked',
  500: 'Internal Server Error'
};

var apiRequest,
    apiRegexp = pathToRegexp('/api/cities/:city?/:id?'),
    storage = {};

function httpError(response, code, message) {
  response.writeHead(code);
  response.end(message || `${code} - ${httpMsg[code]}\n`);
}

function getStaticFile(url, response) {
  var filePath = root + url;
  
  if (url === '' || url === '/') {
    filePath += 'index.html';
  }
  
  try {
    fs.statSync(filePath).isFile();
  }
  catch (error) {
    if (!/^(?=\/?assets).+$/gm.test(url) && url !== '/favicon.ico') {
      filePath = root + '/index.html';
    }
  }

  var extname = path.extname(filePath);
  var contentType = fileTypes[extname] || 'text/html';

  fs.readFile(filePath, function(error, content) {
    if (!error) {
      response.writeHead(200, { 'Content-Type': contentType });
      response.end(content, 'utf-8');
    } else {
      
      switch (error.code) {
        case 'ENOENT':
          console.warn('\x1b[31m', 'Static file '+filePath+' not found.','\x1b[0m');
          httpError(response, 404);
          break;
          
        default:
          httpError(response, 500, 'Sorry, error: '+error.code+' .\n');
          break;
      }
    }
  });
}

function weatherDrift(weather) {

  var result = JSON.parse(JSON.stringify(weather));
  result.date = moment(weather.date).add(1, 'days').format('YYYY-MM-DD');
  
  for(let key in weatherSample) {
    switch(key) {
      case 'temperature':
      case 'pressure':
      case 'humidity':
      case 'windspeed':
      case 'cloudiness':
        result[key] = result[key] + Math.floor(Math.random() * 20 - 10);
        break;

      case 'wind':
        let windIdx = wind.indexOf(result.wind) + Math.floor(Math.random() * 4 - 1.99);
        if (windIdx < 0) {
          windIdx += wind.length - 1;
        } else if (windIdx >= wind.length) {
          windIdx -= wind.length + 1;
        }
        result.wind = wind[windIdx] || 'n';
    }
    
    if ((key === 'humidity' || key === 'cloudiness') && result[key] > 100) {
      result[key] = 100;
    }
    
    if (key === 'windspeed' && result.windspeed < 0) {
      result.windspeed = 0;
    }
  }
  
  result.fall = parseFloat((Math.random() * result.cloudiness / 40).toFixed(3));
  if (result.fall < 0.25) {
    result.fall = 0;
  }
  
  if (result.temperature > 5) {
    result.falltype = 'rain';
  } else if (result.temperature < -3) {
    result.falltype = 'snow';
  } else {
    result.falltype = falltype[parseInt(Math.random() * 3)];
  }
  
  result.storm = result.fall / 4 + Math.random() > 0.5;
  return result;
}

function proceedApi(apiRequest, request, response, body) {
  
  var result = null,
      error = { code: 405, message: httpMsg[405] }, //set default error
      reqParams = {
        city: apiRequest[1],
        date: apiRequest[2]
      };
  
  if (reqParams.city === undefined && request.method === 'GET') { // get cities list
    
    result = { City: citiesList };
    
  } else if (reqParams.date === undefined) { //get weather from city
    
    if (request.method === 'GET') {
      result = { Weather : [] };

      if (storage.hasOwnProperty(reqParams.city)) {
        result.Weather = storage[reqParams.city];
      } else {
        result.Weather.push(weatherDrift(weatherSample));

        for(let i = 1; i<7; i++){
          result.Weather.push(weatherDrift(result.Weather[i-1]));
        }
        storage[reqParams.city] = result.Weather;
      }
    }
  } else if (storage.hasOwnProperty(reqParams.city)) {
    let item =  storage[reqParams.city].find(item => item.date === reqParams.date);
    
    switch (request.method) {
      case 'GET':
        if (item) {
          result = { Weather : item };
        } else {
          error = { code: 404, message: httpMsg[404] };
        }
        break;
        
      case 'PUT':
        if (!item) {
          error = { code: 404, message: httpMsg[404] };
        } else {
          let idx = storage[reqParams.city].indexOf(item);
          storage[reqParams.city][idx] = body;
          result = { status: 'ok', Weather: body };
        }
        break;
        
      case 'POST':
        if (item) {
          error = { code: 423, message: httpMsg[423] };
        } else {
          storage[reqParams.city].push(body);
          result = { status: 'ok', Weather: body };
          response.statusCode = 201;
        }
        break;
    }
  } else {
    error = { code: 404, message: httpMsg[404] };
  }
  
  response.setHeader('Content-Type', fileTypes[".json"]);
  
  if (result === null) {
    console.log(error);
    httpError(response, error.code, JSON.stringify(error.message));
  } else {
    response.end(JSON.stringify(result), 'utf-8');    
  }
}

http.createServer(function (request, response) {
  console.log(request.method, request.url);
  if (request.method === 'OPTIONS') {
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    response.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    response.setHeader('Access-Control-Allow-Credentials', true);  
    response.writeHead(200);
    response.end();
  } else if ((apiRequest = apiRegexp.exec(request.url)) !== null) {
    if (request.method === 'PUT' || request.method === 'POST') {
      let body = '';
      
      request.on('data', data => {
        body += data;
      });

      request.on('end', function () {
        proceedApi(apiRequest, request, response, JSON.parse(body));
      });
    } else {
      proceedApi(apiRequest, request, response);
    }
  } else if (request.method === 'GET') {
    getStaticFile(request.url, response);
  } else {
    httpError(response, 405);
  }
  
}).listen(port, () => {
  console.log('\nServer pid %d running from %s listening port :%d\n', process.pid, __dirname, port);
});

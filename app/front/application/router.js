import { WelcomeComponent } from './components/welcome';
import { AboutComponent } from './components/about';
import { CityComponent } from './components/city';
import { WeatherEditorComponent } from './components/weather-editor';

export const routes = {
  root: [
    {path: '', component: WelcomeComponent},
    {path: 'about', component: AboutComponent},
    {path: ':city', component: CityComponent}
  ],
  child: [
    {path: ':city/:id', component: WeatherEditorComponent}    
  ]
};
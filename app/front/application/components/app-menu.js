import { Component, Input } from '@angular/core';

import { City } from '../models/city';

import template from '../templates/app-menu.html';

@Component({
  selector: 'app-menu',
  template: template
}) export class AppMenu {
  @Input() cities: City[];
  @Input() city: City;
};
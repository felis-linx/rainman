import { Component } from '@angular/core';

import template from '../templates/welcome.html';

@Component({
  selector: 'welcome',
  template: template
}) export class WelcomeComponent {}

import { Component } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import moment from 'moment';

import { StorageService } from '../services/storage';
import { BindingService } from '../services/binding';

import { Weather } from '../models/weather';

import template from '../templates/city.html';

@Component({
  selector: 'city',
  template: template
}) export class CityComponent {
  
  constructor(storage: StorageService, binding: BindingService, route: ActivatedRoute, router: Router) {
    this.storage = storage;
    this.binding = binding;
    
    this.router = router;
    this.route = route;
    
    this.weather = [];
  }

  ngOnInit() {
    
    this.viewSubscription = this.binding.get('viewType').subscribe(viewType => {
      this.viewType = viewType;
    });
    
    this.routeSubscription = this.route.params.subscribe(params => {
      
      if (!this.city || this.city.name !== params.city) {

        if (params.city) {
          this.city = { name: params.city || undefined };
          this.binding.set('city', this.city);
          let res = this.storage.findAll(Weather, this.city.name)
            .then(result => {
              this.weather = result;
            })
            .catch(error => {
              console.warn('[city component] error',error);
            });
        }
      }
    });
  }
  
  ngOnDestroy() {
    this.viewSubscription.unsubscribe();
    this.routeSubscription.unsubscribe();
  }
  
  cardClasses(weather: Weather) {
    let classes = 'card horizontal weather-card ';
    
    if (weather.temperature < -8) {
      classes += 'cold';
    } else if (weather.temperature < 10 ) {
      classes += 'cool';
    } else if (weather.temperature < 25 ) {
      classes += 'heat';
    } else {
      classes += 'hot';
    }
    return classes;
  }
  
  addDay() {
    let lastdate = this.weather[this.weather.length-1].date,
        newDay = new Weather({
      date: moment(lastdate).add(1, 'day').format('YYYY-MM-DD')
    });
    
    console.log('New day', newDay.id, newDay);
    
    this.storage.updateRecord(Weather, newDay, this.city.name)
      .then(result => {
        this.weather.push(newDay);
        this.router.navigate([`/${this.city.name}`, newDay.id]);
      })
      .catch(error => {
        console.warn('[city component] error',error);
      });
  }
}

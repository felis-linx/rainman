import { Component } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';

import { StorageService } from '../services/storage';

import { Weather, wind } from '../models/weather';

import template from '../templates/weather-editor.html';

@Component({
  selector: 'weather-editor',
  template: template
}) export class WeatherEditorComponent {
  
  constructor(storage: StorageService, route: ActivatedRoute, router: Router) {
    
    this.storage = storage;
    
    this.router = router;
    this.route = route;
    
    this.city = '';
    this.weather = {};
  }
  
  ngOnInit() {
    this.routeSubscription = this.route.params.subscribe(params => {
      
      this.city = params.city;
      
      let res = this.storage.findRecord(Weather, params.city, params.id)
        .then((result) => {
          this.weather = new Weather(result);
        })
        .catch((error) => {
          console.warn(error);
          this.router.navigate(['/', this.city]);
        });
    });
  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
  }
  
  cancel() {
    this.router.navigate(['/', this.city]);
  }
  
  update() {
    this.storage.updateRecord(Weather, this.weather, this.city)
      .then(result => {
        this.router.navigate(['/', this.city]);
      })
      .catch(error => {
        console.warn(error);
      });
  }
  
  changeWindDirection() {
    let _wind = wind.indexOf(this.weather.wind) + 1;
    this.weather.wind = wind[_wind >= wind.length ? 0 : _wind];
  }
}

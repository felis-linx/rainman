import { Component } from '@angular/core';

import template from '../templates/about.html';

@Component({
  selector: 'about',
  template: template
}) export class AboutComponent {}

/* exported Injectable, Observable, BehaviorSubject */

import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { BehaviorSubject } from 'rxjs/Rx';

@Injectable()
export class BindingService {
  
  constructor() {
  }

  set(key, data) {
    return this._checkExsistence(key).next(data);
  }

  get(key) {
    return this._checkExsistence(key).asObservable();
  }
  
  _checkExsistence(key) {
    if (!this.hasOwnProperty(key)) {
      this[key] = new BehaviorSubject();
    } else if (!(this[key] instanceof BehaviorSubject)) {
      throw new TypeError(`[Binding service] Unexpected type \`${typeof this[key]}\` of property \`${key}\`.`);
    }
    return this[key];
  }
}
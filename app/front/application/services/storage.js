/* exported Injectable, Http, Response, Headers, Promise */
/* jshint -W052 */
/* jshint unused:false */

import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

@Injectable()
export class StorageService {
  
  constructor(http: Http) {
    this.namespace = '/api';
    this.http = http;
    this.cache = {};
  }
  
  findAll(model, path) {
    return this._find(...arguments);
  }
  
  findRecord(model, path, id) {   
    return this._find(...arguments);
  }
  
  updateRecord(model, data, path) {
    var _path = model._path + (path !== undefined ? path : ''),
        id = data.id,
        cached = this._checkCache(_path, id),
        method = cached ? 'put' : 'post';
    
    this._updateCache(model, data, _path, id);
    
    return this._makeRequest(method, `${_path}/${id}`, data)
      .then(response => response)
      .catch(error => {
        console.warn('[Storage Service] updateRecord', error, ...arguments);
        throw new Error(error);
      });      
  }
  
  _find(model, path, id) {
    var _path = model._path + (path !== undefined ? path : ''),
        cached = this._checkCache(_path, id);

    if (cached) {
      return new Promise((resolve, reject) => {
        let result = (id !== undefined ? cached : Object.keys(cached).map(key => cached[key]));
        resolve(result);
      });
    }
    
    return this._makeRequest('get', _path + (id ? `/${id}` : ''))
      .then(response => this._updateCache(model, response.json(), _path, id))
      .catch(error => {
        console.warn('[Storage Service] _find', error, ...arguments);
        throw new Error(error);
      });
  }
  
  _getHeaders() {
    return new Headers({Accept: 'application/json'});
  }
  
  _makeRequest(method, path, data) {
    let _method = method.toLocaleLowerCase(),
        url = `${this.namespace}/${path}`,
        request;
    
    if (typeof this.http[_method] !== "function") {
      throw new Error('HTTP has`t method '+method);
    }
    
    switch (_method) {
      case 'get':
        request = this.http[_method](url, { headers: this._getHeaders() });
        break;
        
      default:
        request = this.http[_method](url, (data ? JSON.stringify(data) : ''), { headers: this._getHeaders() });
        break;
    }
    return request.toPromise();
  }
  
  _checkCache(path, id) {
    if (this.cache.hasOwnProperty(path)) {
      if (id === undefined && this.cache[path].updated + 120000 > Date.now()) {
        return this.cache[path].store;
      }
      
      if (this.cache[path].store.hasOwnProperty(id)) {
        return this.cache[path].store[id];
      }
    }
  }
  
  _updateCache(Model, data, path, id) {
    let result = [],
        modelName = Model.name,          
        _cacheItem = {
          updated: id ? 0 : Date.now(),
          store: {}
        };

    if (id === undefined) {
      let primaryKey = Model.primaryKey,
          _data = data[modelName] || {};
      
      for(let key in _data) {
        _cacheItem.store[_data[key][primaryKey]] = new Model(_data[key]);
        result.push(_cacheItem.store[_data[key][primaryKey]]);
      }
      this.cache[path] = _cacheItem;
    } else {
      if (!this.cache.hasOwnProperty(path)) {
        this.cache[path] = _cacheItem;
      }
      result = this.cache[path].store[id] = new Model(data[modelName] || data);
      console.log(data);
    }
    console.log(this.cache);
    return result;
  }
}
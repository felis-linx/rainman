/* exported moment */

import { Component, Inject } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';

import moment from 'moment';

import { AppMenu } from './components/app-menu';
import { WelcomeComponent } from './components/welcome';
import { AboutComponent } from './components/about';
import { CityComponent } from './components/city';
import { WeatherEditorComponent } from './components/weather-editor';

import { StorageService } from './services/storage';
import { BindingService } from './services/binding';

import { City } from './models/city';

import template from './templates/main.html';

@Component({
  selector: 'rainman-app',
  template: template
}) export class MainComponent {
  
  constructor(storage: StorageService, binding: BindingService, @Inject('ENVIRONMENT') environment, route: ActivatedRoute, router: Router) {
    this.storage = storage;
    this.binding = binding;
    this.route = route;
    this.router = router;
    
    this.environment = environment;
    this.isDevelopment = environment === 'development';
    
    this.cities = [];
    this.city = { name: 'Select a city'};
    this.viewType = 'module';
    this.binding.set('viewType', this.viewType);
    
    moment.locale('en'); //TODO: i18n. 'ru'
  }

  ngOnInit() {
    this.citySubscription = this.binding.get('city').subscribe(city => {
      if (city) { this.city = city }
    });
    
    let res = this.storage.findAll(City).then(result => {
      this.cities = result;
    });
  }

  ngOnDestroy() {
    this.citySubscription.unsubscribe();
  }
  
  toggleView() {
    console.log('view change');
    this.viewType = this.viewType === 'module' ? 'list' : 'module';
    this.binding.set('viewType', this.viewType);
  }

};

export const COMMON_DECLARATIONS = [MainComponent, AppMenu, WelcomeComponent, AboutComponent, CityComponent, WeatherEditorComponent];
export const COMMON_PROVIDERS = [StorageService, BindingService];

/* jshint unused:false */

import { Model } from './model';
import moment from 'moment';

export const cloudIcons = {
  day: [ 'sunny', 'sunny-overcast', 'cloudy' ],
  night: [ 'clear', 'alt-partly-cloudy', 'alt-cloudy' ]
};

export const wind = ['n', 'ne', 'e', 'se', 's', 'sw', 'w', 'nw'];
export const falltype = ['rain', 'sleet', 'snow'];

export class Weather extends Model {
  
  constructor(wheather) {
    super(...arguments, {
      date: {default: moment().format('YYYY-MM-DD')},
      temperature: {default: 10},
      fall: {default: 0.5},
      falltype: {default: 'rain'},
      pressure: {default: 760},
      humidity: {default: 50},
      wind: {default: 'n'},
      windspeed: {default: 5},
      cloudiness: {default: 50},
      storm: {default: false},
    });    
  }

  static get primaryKey() {
    return 'date';
  }
  
  static get _path() {
    return 'cities/';
  }
  
  get windIcon() {
    return `wi wi-wind wi-wind-from-${this.wind}`;
  }
  
  get weatherIcon() {
    let icon = '';

    if (this.prevTemp !== this.temperature) {
      this.prevTemp = this.temperature;
      if (this.temperature > 5) {
        this.falltype = 'rain';
      } else if (this.temperature < -3) {
        this.falltype = 'snow';
      } else {
        this.falltype = falltype[parseInt(Math.random() * 3)];
      }
    }

    if (this.fall < 0.4) {
      icon += cloudIcons.day[Math.min(parseInt(this.cloudiness/33), 2)];
    } else {
      switch (this.falltype) {
        case 'sleet':
          icon += this.falltype + (this.storm ? '-storm' : '');
          break;
          
        case 'snow':
          icon += this.falltype + (this.storm ? '-thunderstorm' : '');
          break;
          
        default: //rain
          if (this.storm === true) {
            icon += 'thunderstorm';
          } else {
            if (this.fall < 1) {
              icon += 'sprinkle';
            } else if (this.fall < 2) {
              icon += 'showers';
            } else {
              icon += 'rain';
            }
          }
          break;
      }
    }
    if (icon === undefined || icon === '') {
      console.warn('icon failure for '+this.date, this);
      return 'wi-alien';
    }
    return 'wi wi-day-' + icon;
  }
  
}

/* jshint unused:false */

import { Model } from './model';

export class City extends Model {
  
  constructor(city) {
    super(...arguments, {code: {}, name: {default: 'Default city'}});
  }
  
  static get primaryKey() {
    return 'name';
  }
  
  static get _path() {
    return 'cities';
  }
}

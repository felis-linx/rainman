export class Model {
  
  constructor(data, propertyes) {
    for(let key in propertyes) {
      this[key] = data[key] || propertyes[key].default || null;
    }
  }
  
  get id() {
    return this[this.constructor.primaryKey];
  }
  
  static get primaryKey() {
    return null;
  }
  
  static get _path() {
    return null;
  }
}

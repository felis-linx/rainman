/* jshint unused:false */

import 'core-js/client/shim';

import 'rxjs';

import 'jquery';

import 'zone.js/dist/zone';
import 'zone.js/dist/long-stack-trace-zone';

import '@angular/core';
import '@angular/platform-browser-dynamic';
import '@angular/platform-browser';
import '@angular/common';
import '@angular/http';
import '@angular/router';
import '@angular/forms';

import 'materialize-css';
import 'angular2-materialize';
import  'angular2-moment';

import moment from 'moment';

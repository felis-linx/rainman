'use strict';

import { enableProdMode, NgModule } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { FormsModule }   from '@angular/forms';

import { MaterializeDirective } from 'angular2-materialize';
import  { MomentModule } from 'angular2-moment';

import { MainComponent, COMMON_DECLARATIONS, COMMON_PROVIDERS } from './application/main';
import { routes } from './application/router';

if (ENVIRONMENT === 'production') {
  enableProdMode();
}

@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(routes.root, {
      useHash: false
    }),
    RouterModule.forChild(routes.child),
    MomentModule,
    FormsModule
  ],
  declarations: [ MaterializeDirective, COMMON_DECLARATIONS ],
  providers: [{ provide: 'ENVIRONMENT', useValue: ENVIRONMENT }, COMMON_PROVIDERS],
  bootstrap: [ MainComponent ]
}) export class CoreModule {}

platformBrowserDynamic().bootstrapModule(CoreModule);

'use strict';

const gulp = require('gulp'),
      server = require('gulp-develop-server');

const path = require('path'),
      clean = require('del'),
      named = require('vinyl-named');

const concat = require('gulp-concat'),
      gulplog = require('gulplog'),
      gulpIf = require('gulp-if'),
      plumber = require('gulp-plumber'),
      rename = require('gulp-rename'),
      sass = require('gulp-sass'),
      sassUnicode = require('gulp-sass-unicode'),
      cssnano = require('gulp-cssnano'),
      sourcemap = require('gulp-sourcemaps'),
      merge = require('merge-stream');

const webpackStream = require('webpack-stream'),
      webpack = webpackStream.webpack;

const config = require('./config/gulp.conf');

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV.trim() === 'development';

gulp.task('clean', function() {
  return clean([config.destination.root]);
});

gulp.task('front:assets', function() {
  return gulp.src(config.front.assets, {since: gulp.lastRun('front:assets')})
    .pipe(gulp.dest(config.destination.front));
});

gulp.task('front:materialize', function() {
 return merge(
   gulp.src(config.external.materializeCss)
    .pipe(gulpIf(isDevelopment, sourcemap.init()))
    .pipe(sass())
    .pipe(rename('materialize.css'))
    .pipe(gulpIf(isDevelopment, sourcemap.write('.')))
    .pipe(gulpIf(!isDevelopment, cssnano()))
    .pipe(gulp.dest(config.destination.style)),
   
   gulp.src(config.external.materializeFont)
    .pipe(gulp.dest(config.destination.fonts)),
//   gulp.src(config.external.materializeJs)
//    .pipe(gulp.dest(config.destination.js)),
   
   gulp.src(config.external.materialIconCss)
    .pipe(gulpIf(!isDevelopment, cssnano()))
    .pipe(gulp.dest(config.destination.style)),
   
   gulp.src(config.external.materialIconFont)
    .pipe(gulp.dest(config.destination.style)),
   
   gulp.src(config.external.weatherIconCss)
    .pipe(sass())
    .pipe(sassUnicode())
    .pipe(gulpIf(!isDevelopment, cssnano()))
    .pipe(gulp.dest(config.destination.style)),
   
   gulp.src(config.external.weatherIconFon)
    .pipe(gulp.dest(config.destination.font))
 );
});

gulp.task('front:app', function(done) {
  let firstBuildReady = false,
      options = {
        output: {
          filename: '[name].js'
        },
        watch: isDevelopment,
//        devtool: isDevelopment ? 'cheap-module-inline-source-map' : null,
        module:  {
          preLoaders: [{
            test:    /\.js$/,
            include: path.join(__dirname, './app/front'),
            loader:  'jshint-loader',
          }],
          loaders: [
            {
              test:    /\.js$/,
              include: path.join(__dirname, './app/front'),
              loader:  'babel',
              query: { presets: ['es2015', 'angular2'] }
            },
            {
              test: /\.html$/,
              loader: 'raw',
              query: { minimize: false }
            }]
        },
        plugins: [ 
          new webpack.NoErrorsPlugin(),
/*          new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery",
          }),*/
          new webpack.DefinePlugin({ ENVIRONMENT: JSON.stringify(isDevelopment?'development':'production') }),
          new webpack.optimize.CommonsChunkPlugin(
            'vendor', 'vendor.js', Infinity
          )
        ]
      };
  
  if (!isDevelopment) {
    options.plugins.push(new webpack.optimize.UglifyJsPlugin({
      mangle: {except: ['$super', '$', 'exports', 'require']}
    }));
  }

  function wsDone(err, stats) {
    firstBuildReady = true;

    if (err) {
      return;
    }

    gulplog[stats.hasErrors() ? 'error' : 'info'](stats.toString({
      colors: false
    }));

  }
  
  return gulp.src(config.front.app)
    .pipe(plumber())
    .pipe(named())
//    .pipe(named(function() {
//      return 'rainman-app';
//    }))
    .pipe(webpackStream(options, null, wsDone))
    .pipe(gulp.dest(config.destination.js))
    .on('data', function() {
      if (firstBuildReady) {
        done();
      }
    });
});

gulp.task('server:build', function() {
  return gulp.src(config.server, {since: gulp.lastRun('server:build')})
    .pipe(gulp.dest(config.destination.root));
});

gulp.task('server:start', function() {
  return server.listen({path: config.destination.root+'/server.js'});
});

gulp.task('server:restart', function() {
  return server.restart();
});

gulp.task('watcher', function(done) {
  if (isDevelopment) {
    console.log('[watcher] development mode...');
    gulp.watch(config.front.assets, gulp.series('front:assets'));
    gulp.watch(config.server, gulp.series('server:build', 'server:restart'));
//    gulp.watch(config.watch.server, gulp.series('server'));
  }
  done();
});

gulp.task('build', gulp.series(
  gulp.parallel('front:materialize', 'front:assets', 'front:app', 'server:build'),
  'watcher',
  'server:start'
));

gulp.task('default', gulp.series('clean', 'build'));